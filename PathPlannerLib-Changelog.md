# PathPlannerLib v2023.4.2
* Fixed C++ FollowPathWithEvents command requirements checks

# PathPlannerLib v2023.4.1
* Fixed C++ auto builder drivetrain type initialization

# PathPlannerLib v2023.4.0
* Added the ability to add custom logging callbacks to path following commands. Call the static `setLoggingCallbacks` method on the path following command you use to set the logging callbacks.
* Changed path following commands and autobuilders to not mirror paths for the red alliance by default. This can be enabled with the `useAllianceColor` boolean

# PathPlannerLib v2023.3.2
* Automatically transform paths for the red alliance when using PathPlannerLib path following commands or auto builders

# PathPlannerLib v2023.2.3
## Major changes since 2022
* Added new AutoBuilder functionality to generate full autonomous routines from path files
* Added functionality to get event markers for a path. The `getMarkers` method will return a list of all markers on a path. These markers will have a list of names, time, and position associated with them that can be used with path following commands to trigger other code
* Added the ability to load a path as a path group. A path file will be split into multiple paths based on the waypoints marked as stop points. If a path with stop points is loaded as a normal path, the robot will not stop at stop points
* Added support for new "stop events"
* Added holonomic angular velocity to path states
* Added `getConstraintsFromPath` method to allow loading path constraints from a path file. This allows path velocity and acceleration to be changed without rebuilding code
* Added custom `PPHolonomicDriveController` that:
    * Uses PathPlanner paths directly
    * Uses holonomic angular velocity as a feedforward for rotation
    * Automatically configures continuous input for the rotation PID controller
* Added custom `PPSwerveControllerCommand`, `PPMecanumControllerCommand`, `PPRamseteCommand` commands that:
    * Use PathPlanner paths directly
    * Pushes a Field2d and error data to SmartDashboard for debugging
    * Sends path following data over the PathPlannerLib server for visualization within PathPlanner (Java only)
* Added `FollowPathWithEvents` command that will trigger events while running a path following command
* Added `generatePath` methods for on-the-fly generation from a list of points
* Added `getInitialHolonomicPose` method that can be used to reset holonomic odometry at the beginning of a path

### Java Only:
* Added the ability to run a “PathPlannerLib server” (java only for now). When connected to the PathPlanner GUI, this will automatically update paths on the robot when edited in PathPlanner, and will display visualizations in PathPlanner of the current path, target pose, and actual robot pose while path following

## Changes since beta
* Add overloads to generatePath methods to take in a list of points instead of varargs/initializer lists
* Minor fixes and improvements

### Java Only:
* Replace user-facing usages of ArrayList and HashMap with generic List and Map interfaces

### C++ Only
* Add loadPathGroup overload that takes in a vector of PathConstraints instead of an initializer list

# 2023 Beta
## PathPlannerLib Beta v2023.2.0
* Added more constructor options for auto builders to take advantage of the constructor options for path following commands ([#166](https://github.com/mjansen4857/pathplanner/pull/166))
* Added parallel deadline stop event behavior ([#167](https://github.com/mjansen4857/pathplanner/pull/167))
* Added minimum wait behavior ([#168](https://github.com/mjansen4857/pathplanner/pull/168))
* Build with Java 11 compatibility to maintain backwards-compatibility with 2022 WPILib
* Added ability to manually choose control point lengths for on-the-fly generation ([#175](https://github.com/mjansen4857/pathplanner/pull/175))

## PathPlannerLib Beta v2023.1.0
* Added custom path following commands to C++ version
* Added support for multiple events per marker
* Added support for new "stop events"
* Added new AutoBuilder functionality to generate full autonomous routines from path files
* Removed event triggering from path following commands. This was not safe to use in command groups. This has been replaced with the `FollowPathWithEvents` command that will wrap the path following command and run events. This new command is safe to use in groups.

## PathPlannerLib Beta v2023.0.1
* Fixed a PathPlannerLib server crash
* Added `getEndWaitTime` methods to trajectories to get the wait time that was configured in the GUI
* Added `fromCurrentHolonomicState` and `fromCurrentDifferentialState` helper methods to the PathPoint class that will return a PathPoint with the current position, heading, velocity, etc of the robot
* Fixed holonomic rotation not correctly passing the -180/180 barrier
* Added empty PathPlannerTrajectory constructor

## PathPlannerLib Beta v2023.0.0
* Added functionality to get event markers for a path. The `getMarkers` method will return a list of all markers on a path. These markers will have a name, time, and position associated with them that can be used in path following commands to trigger other code
* Added the ability to load a path as a path group. A path file will be split into multiple paths based on the waypoints marked as stop points. If a path with stop points is loaded as a normal path, the robot will not stop at stop points
* Added holonomic angular velocity to path states
* Added `getConstraintsFromPath` method to allow loading path constraints from a path file. This allows path velocity and acceleration to be changed without rebuilding code
* Added custom `PPHolonomicDriveController` that:
    * Uses PathPlanner paths directly
    * Uses holonomic angular velocity as a feedforward for rotation
    * Automatically configures continuous input for the rotation PID controller
* Added custom `PPSwerveControllerCommand`, `PPMecanumControllerCommand`, `PPRamseteCommand`, `PPLTVUnicycleCommand`, `PPLTVDifferentialDriveCommand` commands (Java only for now) that:
    * Use PathPlanner paths directly
    * Automatically triggers commands from given "event map" when reaching event markers
    * Pushes a Field2d and error data to SmartDashboard for debugging
    * Sends path following data over the PathPlannerLib server for visualization within PathPlanner
* Added `generatePath` methods for on-the-fly generation from a list of points
* Added the ability to run a “PathPlannerLib server” (java only for now). When connected to the PathPlanner GUI, this will automatically update paths on the robot when edited in PathPlanner, and will display visualizations in PathPlanner of the current path, target pose, and actual robot pose while path following
* Added `getInitialHolonomicPose` method that can be used to reset holonomic odometry at the beginning of a path
* Other minor fixes and improvements
