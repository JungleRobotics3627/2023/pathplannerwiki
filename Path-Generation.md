<img align="left" width="500" src="https://i.imgur.com/IlFoYQz.png" alt="Path" />

After you are done editing your path, a path file containing all of its information will be automatically saved to your robot code project's deploy directory. When robot code is deployed to the robot, these files can then be loaded with PathPlannerLib in C++ or Java to obtained a generated path that can be followed by autonomous code.

If you would like to use your own generation algorithm, path files use JSON to store every waypoint, and can be easily read using a JSON parser.

## In-App Generation
Paths can also be generated as Pathweaver JSON files or CSV files by enabling your chosen file type in the settings. These files will be auto-generated whenever the path is auto-saved. These pre-generated paths will be located in `{DEPLOY DIRECTORY}/pathplanner/generatedJSON` or `{DEPLOY DIRECTORY}/pathplanner/generatedCSV` depending on the file type. Pathweaver JSON files can be loaded in code the same way as you would load a normal PathWeaver file.