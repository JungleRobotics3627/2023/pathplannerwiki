![Project Menu](https://user-images.githubusercontent.com/9343077/197366600-b42926cb-5716-4c5f-8321-c16aac7b4d08.png)
* **Switch Project Button:** Switch to a different robot code project.
* **Path List:** List of all paths in the current project. Click on a path to start editing that path. Click on the path's name to rename it. Clicking on the three dots button will reveal options to delete or duplicate the path.
* **Add Path Button:** Adds a new path to the project.
* **Settings Button:** Open the settings menu.

## Settings Menu

<img width="400" align="right" alt="image" src="https://user-images.githubusercontent.com/9343077/197366672-58b95f51-f12f-4a87-804d-c82b1a2f00e3.png">

* **Robot Size:**
    * **Robot Width:** Width of the robot in meters from bumper to bumper. Only used for visualization.
    * **Robot Length:** Length of the robot in meters from bumper to bumper. Only used for visualization.
* **Field Image Dropdown:** Select the current field image or import a custom image.
* **Team Color:** Choose a color to change the app's theme to match your team color.
* **PPLib Client:**
    * **Host:** The IP address/hostname of the robot (or simulator).
    * **Port:** Port number of the PathPlannerLib server. This needs to match the port chosen in robot code.
* **Additional Options:**
    * **Generate WPILib JSON:** Enable or disable the generation of Pathweaver JSON files.
    * **Generate CSV:** Enable or disable the generation of CSV files.
    * **Holonomic Mode:** Enable or disable holonomic mode.
    * **PPLib Client:** Enable or disable the connection to a PathPlannerLib server.
