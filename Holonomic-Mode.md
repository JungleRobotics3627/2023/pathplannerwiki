<img align="right" width="400" src="https://i.imgur.com/OO9mECG.png" alt="Holonomic Demo" />

Holonomic mode uses the same generation and pathing as the normal version, but decouples the robot's rotation from the rest of the path. This allows teams with a holonomic drive train (Swerve drive, Mecanum, etc) to have control over the robot's rotation as it follows the path.

When holonomic mode is enabled, the robot's perimeter will be drawn at every point along the path. A small gray dot representing the front of the robot will be drawn on the perimeter. This dot can be dragged to change the robot's rotation at a given point. During generation, the rotation is interpolated between each point.

Holonomic mode can be enabled in the settings of the project menu. Check the [PathPlannerLib documentation](https://github.com/mjansen4857/pathplanner/wiki#pathplannerlib) for info on how to access the holonomic rotation.