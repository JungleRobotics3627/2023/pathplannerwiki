<img align="right" width="600" src="https://user-images.githubusercontent.com/9343077/197116575-04fa3a54-59d8-47f9-9ead-7c22b33b1388.png" alt="Editor Mode Bar" />

PathPlanner has various editor modes that can be selected with the toolbar at the bottom of the app. PathPlanner supports the following editor modes:
* Edit Path
* Preview
* Edit Markers
* Measure
* Graph
* Path Following (Only available when connected to a PathPlannerLib server)

# Edit Path
![image](https://user-images.githubusercontent.com/9343077/197353543-33b85709-b5a5-4637-9c0c-343da660e15a.png)
The "Edit Path" mode is used for editing the position, rotation, and configuration of each waypoint in the path.

Paths consist of two types of points: anchor and control points. Anchor points are points that *anchor* the path. They are points that the path will pass directly through. Control points are points that are attached to each anchor point. Control points can be used to fine-tune the curve of a path by "pulling" the path towards it.

Anchor points, as well as their corresponding control points, can be added by double-clicking anywhere on the field. Any point on the path can be moved by dragging it around. A waypoint can be selected by clicking on its anchor point or control points. This will bring up a waypoint configuration card. If a waypoint is selected when you add a new waypoint, it will be inserted in the path after the selected waypoint. If no waypoint is selected, it will be added to the end of the path.

The selected waypoint will be drawn with an orange outline. Stop points will be drawn with a purple outline.

## Waypoint Configuration Card

<img align="right" width="400" src="https://user-images.githubusercontent.com/9343077/197354850-e2cfd656-61ce-44f9-ab55-9d57d382633e.png" />
The Waypoint Configuration Card is used to manually enter waypoint position/rotation values, as well as other waypoint configuration options. This card can be dragged anywhere within the editor.

* **Lock Button:** If enabled, this will lock a waypoint to prevent it from having its position or rotation changed by clicking and dragging.
* **Delete Button:** Removes this waypoint from the path
* **X Position:** X Position of the waypoint on the field in meters. Simple math expressions can be evaluated by this field: addition `+`, subtraction `-`, multiplication `*`, and division `/`.
* **Y Position:** Y Position of the waypoint on the field in meters. Simple math expressions can be evaluated by this field: addition `+`, subtraction `-`, multiplication `*`, and division `/`.
* **Heading:** Heading of the waypoint on the field in degrees. In holonomic mode, this does not represent the rotation of the robot, but the direction of travel. Simple math expressions can be evaluated by this field: addition `+`, subtraction `-`, multiplication `*`, and division `/`.
* **Vel Override:** Velocity override of the waypoint in meters/second. This can be used to force the robot to slow down at a certain waypoint, or to start/end a path with a non-zero velocity. This is unavailable if the waypoint is a reversal or stop point. Simple math expressions can be evaluated by this field: addition `+`, subtraction `-`, multiplication `*`, and division `/`.
* **Holonomic Rotation:** Rotation of a holonomic robot in degrees at this point in the path. Only available in holonomic mode. This field can be left blank on points that are not the start/end of the path or a stop point. The holonomic rotation will then be determined by the surrounding rotations. Simple math expressions can be evaluated by this field: addition `+`, subtraction `-`, multiplication `*`, and division `/`.
* **Wait Time:** The time to wait at the end of this path. Only available for the end point or stop points. This will not actually cause the robot to wait by itself, it will only let you get the value to be used in a wait command. Useful for waiting between paths in a path group. Simple math expressions can be evaluated by this field: addition `+`, subtraction `-`, multiplication `*`, and division `/`.
* **Reversal:** If enabled, this waypoint will become a reversal. This will cause the robot to come to a stop at this point and reverse away from it.
* **Stop Point:** If enabled, this waypoint will become a stop point. Paths with stop points can be loaded in robot code as a "Path Group", or a list of paths, separated by the stop points.

# Preview
![image](https://user-images.githubusercontent.com/9343077/197353580-f50770e9-a132-41b6-bfba-b69e8b403030.png)
The "Preview" editor mode will show a real-time preview of the current path. This only gives an idea of what the path will look like when run by a robot, as it has no error.

## Generator Settings Card

<img align="right" width="350" src="https://user-images.githubusercontent.com/9343077/197355653-4ac462a5-3127-4c03-b59f-ef23e478b00a.png" />
The Generator Settings Card is used to change the max velocity and acceleration of the path. This card can be dragged anywhere within the editor.

* **Max Velocity:** Max velocity of the path in meters/second.
* **Max Acceleration:** Max acceleration of the path in meters/second^2
* **Reversed:** If enabled, the robot will follow the path reversed, or driving backwards. This setting is unavailable in holonomic mode.

# Edit Markers
![image](https://user-images.githubusercontent.com/9343077/197353729-6c6722b7-ab7d-4492-92e6-a31351318037.png)

The "Edit Markers" editor mode is used to add/edit event markers along the path. These markers can be used to trigger other code upon reaching them while following the path.

## Marker Card

<img align="right" width="250" src="https://user-images.githubusercontent.com/9343077/197356567-8441a576-43b7-4c9a-907e-3716c896e85b.png" />
<img align="right" width="300" src="https://user-images.githubusercontent.com/9343077/197356595-2abe9d10-c361-4613-9405-c29b2a1a0d62.png" />
The Marker Card is used to either add a new marker to the path or edit the selected marker. Markers can be selected by clicking on them, and unselected by clicking on the field.

* **Delete Button:** Delete the selected marker. Only shown if a marker is selected.
* **Events:** Names of the events to trigger. This should match the names in your event map.
* **Position:** Position of the marker. Represented as a "percentage" along the path. For example, a marker halfway between the first two waypoints will have a position of `0.5`. A marker 25% of the way between the second and third waypoint will have a position of `1.25`. The position can also be changed with the slider next to the text box.
* **Add Marker Button:** Adds a marker to the path with the given name and position. Only available if no marker is selected.

## AutoBuilder Stop Event Card

<img align="right" width="250" src="https://user-images.githubusercontent.com/9343077/203180287-54609fe7-6c41-4f9b-a029-3d349203d7ec.png" />
<img align="right" width="250" src="https://user-images.githubusercontent.com/9343077/203180038-a8b4902d-ee1e-469d-a805-92ba1642ad38.png" />

The AutoBuilder Stop Event Card is used to configure the events that the auto builder will run between paths. Stop events are automatically added to every stop point, as well as the start and end point of the path.

* **Execution Behavior:** How the commands will be executed. Either sequential, parallel, or parallel deadline. In the case of parallel deadline, the first event in the events list will be the deadline.
* **Events:** Names of the events to trigger. This should match the names in your event map.
* **Wait Behavior:** Configure the behavior of the wait time option at the stop event. Options are:
    * **None:** There will be no wait at this stop event.
    * **Before:** Wait for the configured wait time, then run the commands.
    * **After:** Run the commands, then wait for the configured wait time.
    * **Deadline:** Run the commands until the wait time elapses. Functions the same as a ParallelDeadlineGroup.
    * **Minimum:** Stop event will take at least the configured wait time to execute, even if the other commands finish executing early.
* **Wait Time:** The wait time for the stop event in seconds.

# Measure
![image](https://user-images.githubusercontent.com/9343077/197353778-5d20a9e0-5789-4aac-8a2a-1569a0bfa9c0.png)
The "Measure" editor mode is used to measure the distance between any two points on the field. Click and drag on the field to create a ruler. The length of the ruler will be displayed within the editor.

# Graph
![image](https://user-images.githubusercontent.com/9343077/197353822-5d58d171-706d-4990-961d-870d3e9eaedc.png)
The "Graph" editor mode will display a graph of the current path for visualization purposes. The graph can display velocity, acceleration, heading/holonomic rotation, angular velocity, and curvature.

## Graph Settings Card

<img align="right" width="400" src="https://user-images.githubusercontent.com/9343077/197366142-de6651c7-f75c-4020-b3ad-ce0950c49e09.png" />

* **Switch Display Button:** Switch the graph display between a sampled path and the full path. The sampled path consists of states every 0.02 seconds apart. This is what the path being followed in robot code will typically look like. The full path will show all states in the original path, so it will be slightly more detailed.
* **Minimize Button:** Minimize or unminimize the graph settings card.
* **Velocity:** Toggle display of the velocity line.
* **Acceleration:** Toggle display of the acceleration line.
* **Heading/Rotation:** Toggle display of the heading/rotation line.
* **Angular Vel:** Toggle display of the angular velocity line.
* **Curvature:** Toggle display of the curvature line.

# Path Following
![image](https://user-images.githubusercontent.com/9343077/197353955-8bdaa8c0-671a-4f39-8092-b64e43c057aa.png)
The "Path Following" editor mode is only available when connected to a PathPlannerLib server. This editor mode will display the path and target pose in gray, and the actual robot pose in white while the robot is following a path. This can be used to judge your path following accuracy.
