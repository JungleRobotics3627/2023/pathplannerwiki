![PathPlanner](https://user-images.githubusercontent.com/9343077/197366867-4e0eb9bb-168e-4673-a3da-2856abe2b483.png)

PathPlanner is a motion profile generator for FRC robots created by team 3015. Every path allows for manual tuning of the robot position and the curve radius at every point. It allows you to create the perfect path for your robot quicker and easier than other generators. PathPlanner can handle more complex paths than other generators because it will slow down the robot as it heads into a turn instead of going through it as fast as possible. If the robot is still not slowing down enough or you would like the robot to go slow at a certain point in the path, the robot's max velocity can be overridden at each point. Paths are generated in robot code with and easy to use vendor library, PathPlannerLib.

# Using PathPlanner
- [Controls & Shortcuts](https://github.com/mjansen4857/pathplanner/wiki/Controls-&-Shortcuts)
- [Project Menu](https://github.com/mjansen4857/pathplanner/wiki/Project-Menu)
- [Editor Modes](https://github.com/mjansen4857/pathplanner/wiki/Editor-Modes)
- [Holonomic Mode](https://github.com/mjansen4857/pathplanner/wiki/Holonomic-Mode)
- [Path Generation](https://github.com/mjansen4857/pathplanner/wiki/Path-Generation)

# PathPlannerLib
- [Installing](https://github.com/mjansen4857/pathplanner/wiki/PathPlannerLib:-Installing)
- [Java Usage](https://github.com/mjansen4857/pathplanner/wiki/PathPlannerLib:-Java-Usage)
- [C++ Usage](https://github.com/mjansen4857/pathplanner/wiki/PathPlannerLib:-Cpp-Usage)