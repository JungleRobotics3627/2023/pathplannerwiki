**PathPlannerLib server is currently only available in the Java version.**

# Using the PPLib server
All you need to do to enable the PPLib server in robot code is call this method (for example in the `robotInit` method):
```Java
PathPlannerServer.startServer(5811); // 5811 = port number. adjust this according to your needs
```

If using PathPlannerLib path following commands, path following information will automatically be sent over the server. If you are using custom path following, you can use the `PathPlannerServer.sendActivePath` and `PathPlannerServer.sendPathFollowingData` methods.

After starting the server on the robot, enable the client connection in the PathPlanner app and configure the host and port number settings to match the host address of your robot (usually `roboRIO-####-FRC.local` or `10.##.##.2` where #### is your team number, or `localhost` if running a simulator) and the port number you chose above. PathPlanner will automatically connect to the server and the Path Following [Editor Mode](https://github.com/mjansen4857/pathplanner/wiki/Editor-Modes#path-following) will become available. Run a path following command to watch the visualization!