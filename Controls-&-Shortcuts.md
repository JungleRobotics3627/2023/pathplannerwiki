| Shortcut                                     | Description                                    |
|----------------------------------------------|------------------------------------------------|
| Left Click + Drag                            | Move Point                                     |
| Double-Click on Field                        | Add Waypoint                                   |
| Click on Waypoint                            | Open Waypoint Configuration Menu               |
| Ctrl/⌘ + Z                                   | Undo                                           |
| Ctrl/⌘ + Y                                   | Redo                                           |

### UI Controls
- The project menu can be opened by using the hamburger menu button in the top left corner of the app
- If you are connected to the robot, robot code can be deployed using the action button in the bottom right corner. This allows for quick testing and iteration. This functionality is not supported on builds downloaded from the Apple App Store.
- Scroll on the field to zoom in/out
- Change the editor mode using the toolbar at the bottom-center of the editor.